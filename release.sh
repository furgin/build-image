#!/usr/bin/env bash

npm install -g bats

./test.bats

registry="registry.furgin.org"
version="latest"
image="${registry}/furgin/build-image:${version}"

docker build -t ${image} .
docker login -u "$DOCKER_USER" -p "$DOCKER_PASSWORD" "${registry}"
docker push "${image}"
