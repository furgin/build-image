#!/usr/bin/env bats

BITBUCKET_DOCKER_HOST_INTERNAL=${BITBUCKET_DOCKER_HOST_INTERNAL:-"172.17.0.1"}

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/build-image"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "python available" {
    run_pipe_container python --version
    echo "Output[$status]: $output"
    [[ "$status" -eq 0 && "$output" == "Python 3.8.2" ]]
}

@test "pip available" {
    run_pipe_container pip --version
    echo "Output[$status]: $output"
    [[ "$status" -eq 0 && "$output" == "pip 20.0.2"* ]]
}

@test "docker available" {
    run_pipe_container docker --version
    echo "Output[$status]: $output"
    [[ "$status" -eq 0 && "$output" == "Docker version 19.03.12, build 48a66213fe" ]]
}

@test "git available" {
    run_pipe_container git --version
    echo "Output[$status]: $output"
    [[ "$status" -eq 0 && "$output" == "git version 2.25.1" ]]
}

run_pipe_container() {
  FIXTURE_DIR="$(pwd)"

  echo $DOCKER_IMAGE

  run docker container run \
    --volume=/usr/local/bin/docker:/usr/local/bin/docker:ro \
    --volume=/var/run/docker.sock:/var/run/docker.sock \
    --env=DRY_RUN="true" \
    --env=DEBUG="false" \
    --env=DOCKER_HOST="$DOCKER_HOST" \
    --add-host="host.docker.internal:$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --env=BITBUCKET_DOCKER_HOST_INTERNAL="$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --workdir=$FIXTURE_DIR \
    --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    --env=BITBUCKET_CLONE_DIR="$FIXTURE_DIR" \
    --env=BITBUCKET_REPO_OWNER="furgin" \
    --env=BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
    --env=BITBUCKET_COMMIT="$BITBUCKET_COMMIT" \
    --env=BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
    ${DOCKER_IMAGE}:test \
    "$@"
}


