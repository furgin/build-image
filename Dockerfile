FROM ubuntu:latest

RUN apt-get update \
    && apt-get -y install \
        python3 \
        bash \
        curl \
        python3-distutils \
        python3-apt \
        python3-pip \
        git \
    && apt-get clean

RUN ln -s /usr/bin/pip3 /usr/bin/pip
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN pip install semversioner

RUN curl -sL https://deb.nodesource.com/setup_lts.x | bash -
RUN apt-get install -y nodejs
RUN npm install -g yarn
